import React, { Component } from 'react';
import LoginForm   from './LoginForm';
import { Redirect } from 'react-router';
//import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  constructor(props)
  {
    super();
  } 
  render() {
    const current_user = JSON.parse(localStorage.getItem('current_user'));
    if(current_user === null)
    {
      return (
      <div className="App">
        <header className="App-header">          
          <h1 className="App-title">Welcome to Notes Application</h1>
        </header>
        <LoginForm />
      </div>
      );
    } 
    else
    {
      return(<div>
                   <Redirect to="/Home" /> 
            </div>);
    }  
  }
}
export default App; //Redirect tag also maintain global state