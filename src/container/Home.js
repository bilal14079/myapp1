
import React, { Component } from 'react';
import Popup from "reactjs-popup";
import * as userActions from '../actions/userActions';
import { bindActionCreators } from 'redux' ;
import {connect} from 'react-redux';
import { Button,Navbar,Nav,NavItem } from 'react-bootstrap';
import {Link} from "react-router-dom";



class Home extends Component
{
  constructor(props)
  {
    super(props);
    this.state={ notes : [] ,popUpContent : '' ,viewtotalNotes: false ,noteDeleted :false ,noteUpdated :false ,noteAdded :false};
    this.addtoNotesList=this.addtoNotesList.bind(this);
    this.makeEdit = this.makeEdit.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.handleChange=this.handleChange.bind(this);
    this.getLogout = this.getLogout.bind(this);
  }
  handleChange(event)
   {
    this.setState({ popUpContent : event.target.value  })
   }
  makeEdit(note)
  {
    let newDetails= this.state.popUpContent;
    this.props.editNotes(note,newDetails);
    this.setState( { noteUpdated :true} );
  }
  doDelete(note)
  {
    this.props.deleteNotes(note);
    this.setState({ noteDeleted :true });     
  }
  addtoNotesList(title,details)
  {
    let note= {'title': title, 'details': details}   
    this.props.createNotes(note);
    this.setState({ noteAdded :true })
  }
  getLogout()
  {
    this.props.getLogOut();
  }

  render()
  {
   return(        
      <div> 
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
               <Link to={'/Home'}>Home</Link> 
            </Navbar.Brand>
          </Navbar.Header>
          <Nav>
              <NavItem eventKey={1}  >
                  <Link to={'/showAllNotes'}>View All Notes</Link>
              </NavItem>
              <NavItem eventKey={1} onClick={this.getLogout} >
                 LogOut
              </NavItem>
          </Nav>
        </Navbar>
          <h1 className="text-success">Welcome to home page </h1>
              <CreateNotes addtoNotesList={this.addtoNotesList}  />
          <div> <h3 > List of Notes from active author </h3> </div>
              <ViewNotes notes={this.state.notes} makeEdit={this.makeEdit} doDelete={this.doDelete}  popupContent={this.state.popUpContent} handleChange={this.handleChange} users_List={this.props.users_List}  />    
      </div>       
    );
  }
}

export class CreateNotes extends Component
{
  constructor(props)
  {
    super(props);
    this.state = { title: '' , details: '' };
    this.handleChange=this.handleChange.bind(this);
    this.handlesubmit=this.handlesubmit.bind(this);
  }
  handleChange(event)
   {
    let obj = {}
    obj[event.target.name] = event.target.value
    this.setState(obj)
   }    
    handlesubmit(event)
    {
      event.preventDefault();
      this.props.addtoNotesList(this.state.title,this.state.details);     
    }
  render()
  {
     return(
      <div className="Form">
        <form onSubmit={this.handlesubmit}>
          <div className="form-group">
            <input className="form-control" placeholder="Enter Title..." type="text"  name="title" value={this.state.title} onChange={this.handleChange} />
          </div>
          <div className="form-group">
               <label for="comment">Enter Details of notes:</label>
            <textarea className="form-control" rows="5"  name="details"  value={this.state.details} onChange={this.handleChange} ></textarea>
          </div>
          <div className="form-group">
              <input className="btn btn-success" type="submit" value="Create Note" />
          </div>
        </form>
      </div>
      );
  }
}
//display all notes of active user
function ViewNotes(props) 
{
  const current_user = JSON.parse(localStorage.getItem('current_user'));
  const usersList = props.users_List;
  var notes = null;
  for(let i=0 ; i< usersList.length ; i++)
  {
      if(current_user.userEmail === usersList[i].email && current_user.userPassword === usersList[i].pwd )
      {
        notes = [...usersList[i].userNotes];
        break;
      }    
   }
  const notesList = notes.map((note) =>
    <li key={note.title}> {note.title}: {note.details}
      <Popup trigger={ <Button bsStyle="success" > Edit </Button> } modal closeOnDocumentClick  >         
        <input type="text" name="popUpContent" value={props.popUpContent} placeholder="Enter New Details here..." onChange={props.handleChange} /> 
        <Button bsStyle="info" onClick={ () => {props.makeEdit(note)} } >Update </Button>      
      </Popup>       
        <Button bsStyle="danger"onClick={ () => { props.doDelete(note)} } >Delete</Button>  
    </li> 
  );
  return (
    <ol className="text-info">{notesList}</ol> 
  );
}
const mapStateToProps = (state) => {
  return{
    users_List: state.user.usersList,
    };
};
const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({...userActions }, dispatch) 
});

export default connect(mapStateToProps,mapDispatchToProps)(Home);
