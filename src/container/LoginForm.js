
import React, { Component } from 'react';
import {connect} from 'react-redux';
import getLogin from '../actions/userActions';
import { bindActionCreators } from 'redux' ;

class LoginForm extends Component
{
  constructor(props)
  {
    super(props);
    this.state={
      email:'',
      pass:'',    
      isUserAuthenticated : false,msg: null    
    };
    this.handlesubmit=this.handlesubmit.bind(this);
    this.handleChange=this.handleChange.bind(this);
    localStorage.setItem("isUserAuthenticated", JSON.stringify(false));
  }
  handleChange(event)
  {
    let obj = {};
    obj[event.target.name] = event.target.value;
    this.setState(obj);
  }
  handlesubmit(event)
  {
    event.preventDefault();
    this.props.getLogin(this.state.email, this.state.pass); 
  }
  
  render()
  {    
    return(
      <div className="Form">
          <center>
            <h4 className="text-info">Fill up the signIn form to get login </h4>
            <form onSubmit={this.handlesubmit}>
              <div className="form-group">
                <input className="form-control" type="text" id="email" name="email" value={this.state.email} placeholder="enter email" onChange={this.handleChange } required/>
              </div>
              <div className="form-group">
                <input className="form-control" type="text" id="pass" name="pass" value={this.state.pass} placeholder="enter password" onChange={this.handleChange} required/>
              </div>
              <div className="form-group"> 
                <p>{this.props.errorMsg} </p>
              </div>
              <div className="form-group"> 
                <input  className="btn btn-primary" type="submit" value="Login" />
              </div>
            </form>          
          </center>
      </div>
  );
  } 
}
const mapStateToProps = (state) => {
  return{
    errorMsg: state.form.errorMsg ,
  };
};
const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getLogin
  },
  dispatch,
)

export default connect(mapStateToProps,mapDispatchToProps)(LoginForm);



