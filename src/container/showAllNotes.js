import React,{Component} from 'react';
import {connect} from 'react-redux';

class ViewAllNotes extends Component
{

	render()
	{
		let viewallNotes =[]; //to show all notes
	    let notes =null;      
      const userList = this.props.user_List;
	    for(let i=0 ;i < userList.length ;i++)
	    {
	       notes=[...userList[i].userNotes];
	       viewallNotes.push(<ViewNotes key ={i} userNotesList={notes} authorName={userList[i].email} /> );
	    }   	    
   		return (
      		<div> 
       		 	<h1>List of Notes by all Authors</h1>
         		{viewallNotes}
    	   	</div>
    );    
	}  
}
function ViewNotes(props)
{
    let notes = props.userNotesList;
    let notesList = notes.map((note) =>
   <li key={props.key}> {note.title}: {note.details} </li>);
   return (
      <div> 
        <hr />
      		<h3>Notes by Author : {props.authorName}</h3>             
         	<ol className="text-info">{notesList} </ol> 
     </div>
    );    
}
const mapStateToProps = (state) => {
  return{
    user_List: state.user.usersList
  };
};

export default connect(mapStateToProps)(ViewAllNotes);
