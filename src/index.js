import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './container/App';
import {Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore,combineReducers,applyMiddleware,compose} from "redux";
import userReducer from './reducers/userReducer';
import formReducer from './reducers/formReducer';
import thunk from 'redux-thunk';
import Home from './container/Home' ;
import Login from './Login' ;
import ViewAllNotes from './container/showAllNotes'
import createHistory from 'history/createBrowserHistory'
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware,
} from "react-router-redux";



const userList = JSON.parse(localStorage.getItem('userlist'));

if(userList === null)
{
  let userlist = [{ email : 'bilal@gmail.com' , pwd: '123' , userNotes :[] } , 
        { email : 'ali@gmail.com' , pwd: '123' , userNotes : [] } ,
            {email  : 'ahmad@gmail.com' , pwd: '123' ,userNotes : [] } ] ;

localStorage.setItem("userlist", JSON.stringify(userlist));
}

const rootReducer = combineReducers({
    form:formReducer,
    user: userReducer,
    router:routerReducer 
});
export const history = createHistory();
const middleware = [thunk,routerMiddleware(history)];

const store = createStore(rootReducer,compose( applyMiddleware(...middleware),
	 window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	 )
);

ReactDOM.render(
(<Provider store= {store}>
	<ConnectedRouter history={history}>
	   <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/Home" component={Login(Home)} />
        <Route exact path="/showAllNotes" component={Login(ViewAllNotes)} />
     </Switch>
  </ConnectedRouter>
</Provider>),document.getElementById('root'));

