//import React from "react";
//import { Redirect } from 'react-router';


const userList =JSON.parse(localStorage.getItem('userlist'));
const userReducer = ( state = {current_user : null,usersList : userList} , action ) => {
	console.log("In user reducer");
	//console.log("store state",store.getState());

	switch(action.type)
	{
		case "GETLOGIN" :
		return state;		
		//break;
		case "LOGIN_SUCCESS" :
		return Object.assign({}, state, {
		 			isUserAuthenticated : action.isUserAuth,
		 			current_user : action.curr_user,
		 			usersList : userList
     			 	});		
		case "GET_LOGOUT" :
		return {...state,current_user:null , isUserAuthenticated :false };
		//break;

		case "CREATENOTES" :
		return {...state,usersList: action.users_List};
		//break;

		case "EDIT_ACTIVE_USER_NOTES" :
		return {...state,usersList: action.users_List};
		//break;

		case "VIEW_ACTIVE_USER_NOTES" :
		return state;
		//break;
		case "DELETE_ACTIVE_USER_NOTES" :
		return {...state,
				usersList: action.users_List};
		//break;
		
		case "VIEW_ALL_NOTES" :
		return state;

		default:
		return state;
	}

}
export default userReducer;