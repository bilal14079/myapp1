import React, { Component } from 'react';
import {Redirect} from 'react-router';

export default function Login(RenderComponent) //it is higher order component for login validation for any page/component that you want
{
	return class Login extends Component 
	{
		constructor(props)
		{
			super(props);
			let isuserAuthenticated = JSON.parse(localStorage.getItem('isUserAuthenticated'));
			this.state = { isUserAuthenticated : isuserAuthenticated }
		}

		render() 
		{
			if(this.state.isUserAuthenticated)
			{
				return ( <RenderComponent /> );
			}
			else
			{
				return ( <Redirect to='/' /> );
			}    		
    }
		}
  
}
