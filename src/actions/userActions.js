import {history} from '../index.js';
export default function getLogin(user_Email,user_Password)
{
  return function (dispatch,state){
	var isUserAuthentic = false;
	let current_user=null;
	console.log("In get login action");
	const usersList = JSON.parse(localStorage.getItem('userlist'));
 	for(let i=0 ; i< usersList.length ; i++)
	{
  		if(user_Email=== usersList[i].email && user_Password === usersList[i].pwd )
  		{
  			current_user = { userEmail :user_Email , userPassword: user_Password  };
    		localStorage.setItem("isUserAuthenticated", JSON.stringify(true));
    		localStorage.setItem("current_user", JSON.stringify(current_user));
    		isUserAuthentic = true;
        dispatch(loginSuccess(isUserAuthentic,current_user)); //thunk /async call to other function 
        history.push('/Home');
    		break;   		
		  }
  	  else if(i === usersList.length-1)
 		  {
        dispatch(showErrorMessage());
 			  //history.push('/?msg=InvalidCredentials!');
 		  }
    }
  }
}
export function showErrorMessage()
{
  return{
    type : 'SETERRORMESSAGE',
    errorMsg: "InvalidCredentials!"
  }

}
export function getLogOut()
{
	localStorage.removeItem('current_user');
	localStorage.setItem("isUserAuthenticated", JSON.stringify(false));
  history.push('/');
	return{
		type : 'GET_LOGOUT',
	}

}

export function createNotes(note)
{
  return function(dispatch,state)
  {
    const usersList = state().user.usersList;
    const current_user = JSON.parse(localStorage.getItem('current_user'));
    for(let i=0 ; i< usersList.length ; i++)
    {
      if(current_user.userEmail === usersList[i].email && current_user.userPassword === usersList[i].pwd )
      {
        let NotesCopy = [...usersList[i].userNotes];
        NotesCopy.push(note);
        usersList[i].userNotes = NotesCopy;
        localStorage.setItem("userlist", JSON.stringify(usersList));
        dispatch(noteAdded(usersList));       
        break;
      }    
    }
  }
}
export function noteAdded(userlist)
{
  return {type : 'CREATENOTES',
  users_List: userlist}
}
export function loginSuccess(isUserAuthentic,current_user)
{
	return {
		type : 'LOGIN_SUCCESS',
		isUserAuth : isUserAuthentic,
    curr_user : current_user
	}
}

export function editNotes(note,newDetails)
{
  return function(dispatch,state)
  {
    const usersList = state().user.usersList;
    const current_user = JSON.parse(localStorage.getItem('current_user'));
    let noteslist=null;
    for(let i =0 ; i< usersList.length ;i++)
    {
        if( usersList[i].email === current_user.userEmail )
        {
          noteslist = [...usersList[i].userNotes];
          for(let j =0 ; j< noteslist.length ;j++)
          {
            if( noteslist[j].title === note.title && noteslist[j].details === note.details )
            {
              noteslist[j].details =newDetails;
              usersList[i].userNotes=noteslist;
              localStorage.setItem("userlist", JSON.stringify(usersList));
              dispatch(noteEditted(usersList));
              break;
            }
          }
          break;
        }
    }
  }
}
export function noteEditted(usersList)
{
  return {
    type : 'EDIT_ACTIVE_USER_NOTES' ,
    users_List: usersList
  }
}
export function deleteNotes(note)
{
  return function(dispatch,state)
  {
    const usersList = state().user.usersList;
    const current_user = JSON.parse(localStorage.getItem('current_user'));
    for(let i=0 ; i< usersList.length ; i++)
    {
      if(current_user.userEmail === usersList[i].email && current_user.userPassword === usersList[i].pwd )
      {
        let noteslist = [...usersList[i].userNotes];
        let index = noteslist.indexOf(note);
        noteslist.splice(index, 1);
        usersList[i].userNotes = noteslist;       
        localStorage.setItem("userlist", JSON.stringify(usersList));
        dispatch(noteDeleted(usersList));
        break;
      }
    }
  }
	
}
export function noteDeleted(usersList)
{
  return {
    type : 'DELETE_ACTIVE_USER_NOTES' ,
    users_List: usersList
  }
}
